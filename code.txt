// ==UserScript==
// @name        Add Dissent Button
// @description Add Dissent Button
// @include       *
// @grant       GM_addStyle
// @require http://code.jquery.com/jquery-latest.js
// ==/UserScript==

$(document).ready(function() {
    var mySidenav = document.createElement ('div');
    mySidenav.setAttribute ('id', 'mySidenav');
    mySidenav.setAttribute ('class', 'sidenav');
    document.body.appendChild (mySidenav);

    var openSpan = document.createElement("div");
    openSpan.innerHTML = '<a href="javascript:void(0)" class="dissenterbtn">Dissent This</a>';
    document.body.appendChild (openSpan);

    if ( window.location !== window.parent.location ) {
        $( ".dissenterbtn" ).css('display', 'none')
    } else {
        $( ".dissenterbtn" ).click(function() {
            $( ".sidenav" ).toggle();
            mySidenav.innerHTML = '<iframe style="z-index: 1000 !important; height: 100% !important; width: 100% !important; border: 0 !important;" src="https://dissenter.com/discussion/begin-extension?url=' + window.location.href + '"></iframe>';
        });
    }

});


GM_addStyle ( `

.sidenav {
   position: absolute !important;
   display: none;
   top:0 !important;
   left:0 !important;
   z-index:2000 !important;
   width: 400px !important;
   height:80% !important;
   background: #272B30 !important;
}

.dissenterbtn {
   background: #21CF7B !important;
   border-radius: 2px !important;
   padding:5px !important;
   position: fixed !important;
   bottom: 22px !important;
   left: 0 !important;
   font-size: 12px !important;
   text-align: center !important;
   line-height: 20px !important;
   color: #fff !important;
   z-index: 1000 !important;
}

` );